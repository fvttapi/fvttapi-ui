import {createSlice} from '@reduxjs/toolkit'

const chatSlice = createSlice({
    name: 'chat',
    initialState: {
        messages: [],
    },
    reducers: {
        setChatMessagesSuccess: (state, action) => {
            if (action.payload !== undefined)
                state.messages = action.payload;
        }
    },
});

export const setChatMessages = (data) => async (dispatch) => {
    dispatch(setChatMessagesSuccess(data));
}


export const {
    setChatMessagesSuccess,
} = chatSlice.actions

export default chatSlice.reducer
