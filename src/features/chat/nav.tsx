import React from 'react';
import ProtectedNavItem from "../../components/nav/NavLeft/ProtectedNavItem";
import NavItem from "../../components/nav/NavLeft/NavItem";
//            <ProtectedNavItem to={'/chat'} primary={'Chat'} />

export default function NavLeftChat() {
    return (
        <>
            <NavItem to={'/chat'} primary={'Chat'} />
        </>
    );

}

