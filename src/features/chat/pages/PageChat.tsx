import React, {useEffect, useState} from 'react';
import FullPageLayout from "components/FullPageLayout";
import {Box, Button, Fade, styled, Typography} from "@mui/material";
import {useGetAllServersQuery, wireguardApi} from "../../auth/apiSlice";

const PREFIX = 'PageChat';
const classes = {
    errorBox: `${PREFIX}-errorBox`,
    loadingBox: `${PREFIX}-loadingBox`,
    testcss: `${PREFIX}-testcss`,
};

const StyledFullPageLayout = styled(FullPageLayout)(({theme}: any) => ({
    [`& .${classes.errorBox}`]: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'middle',
    },
    [`& .${classes.loadingBox}`]: {
        //display: 'flex',
        justifyContent: 'center',
        alignItems: 'middle',
    },
    [`& .${classes.testcss}`]: {
        backgroundColor: 'rgba(255,0,0,0.5) !important',
    },
}));

export default function PageChat(props: any) {
    // @ts-ignore
    //const { data: servers, error, refetch, isFetching } = useGetAllServersQuery();

    const [connected, setConnected] = useState<boolean>(false);


    return (
        <StyledFullPageLayout title={'Chat'} header={'Chat'} className={classes.testcss}>
            stuff
        </StyledFullPageLayout>
    );

}
