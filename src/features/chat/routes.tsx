import React from "react";
import ProtectedRoute from "components/ProtectedRoute";
import PageChat from "./pages/PageChat";
import {Route} from "react-router-dom";

//<ProtectedRoute path='/chat' exact><PageChat/></ProtectedRoute>

const ChatRoutes = (
    <>
        <Route path='/chat' exact><PageChat/></Route>
    </>
)

export default ChatRoutes;


