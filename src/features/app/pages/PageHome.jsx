import React, {useEffect} from 'react';
import Typography from "@mui/material/Typography";
import FullPageLayout from "components/FullPageLayout";
import Divider from "@mui/material/Divider";
import {Link} from 'react-router-dom';
import {logout} from "../../auth/authSlice";
import {connect} from "react-redux";


function PageHome(props) {

    const { isAuthenticated } = props;

    useEffect(() => {
    }, []);

    return (
        <FullPageLayout
            header={'FoundryVTT API'}
            title={'Dashboard'}
        >
            <Typography variant={'body1'}>Welcome to FoundryVTT external API.</Typography>
            <Divider sx={{my: 2}}/>
            {isAuthenticated && <>
                <Typography variant={'body1'} sx={{mb: 1}}>This app is still heavily under development (interface-wise).</Typography>
            </>}
            {!isAuthenticated && <>
                <Typography variant={'body1'} sx={{mb: 1}}>You must login to use this app.  You may login in the sidebar on the left.</Typography>
            </>}
        </FullPageLayout>
    );

}

const mapStateToProps = (state) => ({
    isAuthenticated: state.auth.isAuthenticated,
})

const mapDispatchToProps = {
    logout,
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PageHome)

